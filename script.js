var l_time;

var svg = (element) => {
    return document.createElementNS('http://www.w3.org/2000/svg', element);
}

var padZero = (i) => {
    if (i <= 9) {
        return '0' + i;
    } else {
        return i;
    }
}

var start_clock = (fast, intval, clock) => {
  if (intval == 0) {
    intval = 1000
  }
  return setInterval(() => {
        t = new Date()
        
        if (fast == true) {
          s = s+1;
          if (s ==60) {
            s=0;
            m = m+1;
            if(m == 60) {
              m=0
              h=h+1
              if (h == 24) h = 1
            }
          }
        } else {
          s=t.getSeconds()
          m=t.getMinutes()
          h=t.getHours()
        }
        
        clock.secs.text(s)
        
        pos_cord = (2 * Math.PI / 60 ) * (m - 15) +0.03
        clock.min_circ.attr({cx: ((Math.cos(pos_cord) * .53)), cy: ((Math.sin(pos_cord) * .53))})
        
        pos_cord = (2 * Math.PI / 60 ) * (((h % 12) * 5)- 15 + (m/12)) +.03;
        clock.hrs_circ.attr({cx: ((Math.cos(pos_cord) * .83)), cy: ((Math.sin(pos_cord) * .83))})
        
    }, intval, clock, fast)
}

$(document).ready(() => {
    
    var svg_container = $('#svg')
   /* var hrs_points = [], min_points=[] */
    var hrs_text = [], min_text=[]
    
    // setting the hours 
    for (i=0; i<12 ;i++) {
            pos_cord = 2 * Math.PI / 12 * (i-2);
           /* hrs_points[i] = $(svg('circle'))
            hrs_points[i].attr({fill: '#fff', cx: (Math.cos(pos_cord) * .93).toString(), cy: (Math.sin(pos_cord) * .93).toString(), r: '1%'}) */
            hrs_text[i] = $(svg('text'))
            hrs_text[i].attr({x: ((Math.cos(pos_cord) * .83)), y: ((Math.sin(pos_cord) * .83)+.05), class: 'hrs'}).text(i+1)
    }
    
    svg_container.append(/*hrs_points,*/ hrs_text)
    
    
    // setting the minutes 
    for (i=0, j=0; i<60 ;i++) {
            pos_cord = 2 * Math.PI / 60 * (i-15);
         /*   min_points[i] = $(svg('circle'))
            min_points[i].attr({fill: '#fff', cx: (Math.cos(pos_cord) * .93).toString(), cy: (Math.sin(pos_cord) * .93).toString(), r: '.5%'})*/
            if(i%5 == 0) {
            
           min_text[j] = $(svg('text'))
           min_text[j].attr({x: ((Math.cos(pos_cord) * .53)-.06), y: ((Math.sin(pos_cord) * .53)+.05), class: 'mins'}).text(padZero(i))
           j++

           } 
    }
    svg_container.append(/*min_points,*/ min_text)
    
    // seconds text
    var t = new Date()
    var secs = $(svg('text'))
    secs.attr({x: '-.06', y: '0', class: 'secs' }).text(t.getSeconds())
    
    // minutes circle
    pos_cord = (2 * Math.PI / 60 * (t.getMinutes() - 15)) + 0.03;
    
    min_circ = $(svg('circle'))
    min_circ.attr({fill: 'rgba(0,0,0,0)', cx: ((Math.cos(pos_cord) * .55)), cy: ((Math.sin(pos_cord) * .53)), r: '7%', stroke: '#fff', 'stroke-width': '.01'})
    
    // hours circle
    pos_cord = (2 * Math.PI / 60 ) * (((t.getHours() % 12) * 5) - 15 + (t.getMinutes()/12)) +.03; 
    
    hrs_circ = $(svg('circle'))
    hrs_circ.attr({fill: 'rgba(0,0,0,0)', cx: ((Math.cos(pos_cord) * .77)), cy: ((Math.sin(pos_cord) * .83)), r: '5%', stroke: '#fff', 'stroke-width': '.01'})
    
    
    svg_container.append(secs, min_circ, hrs_circ)

    l_time = start_clock(false,1000, {secs, min_circ, hrs_circ});
    
    $('#pause').on('click', () =>{
      clearInterval(l_time)
    })
    
    $('#start').on('click', () => {
      clearInterval(l_time)
      l_time = start_clock(false, 1000,  {secs, min_circ, hrs_circ})
    })
    $('#faster').on('click', () => {
      
      speed = 10;
      clearInterval(l_time)
      l_time = start_clock(true, speed, {secs, min_circ, hrs_circ})
    })
});